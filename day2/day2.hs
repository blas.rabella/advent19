{-# LANGUAGE ScopedTypeVariables, FlexibleContexts #-}

import Data.List.Split (splitOn)
import Data.Array.ST
import Data.Array.Unboxed

main :: IO ()
main = do
  input :: [Int] <- (map read . splitOn ",") <$> getContents
  let a = solve1 input 12 2 
  let (value, (noun, verb)) = solve2 input
  print a
  print $ (100 * noun) + (verb) 

solve1 :: [Int] -> Int -> Int -> Int
solve1 input a0 a1 = a ! 0
  where a = runSTUArray $ do
        prg <- newListArray (0, (length input) - 1) input
        writeArray prg 1 a0
        writeArray prg 2 a1
        run 0 prg

solve2 :: [Int] -> (Int, (Int, Int))
solve2 input = head . dropWhile (\(v, _) -> v /= 19690720) $ solutions
  where l = (length input) - 1
        finalValues = (solve1 input) <$> [0..l] <*> [0..l]
        pairs = (,)  <$> [0..l] <*> [0..l]
        solutions = zip finalValues pairs
        
run :: (MArray a1 Int m) => Int -> a1 Int Int -> m (a1 Int Int)
run ptr prg = do
  instr <- readArray prg ptr
  if instr == 99
    then do return prg
    else do
      aAddr <- readArray prg (ptr + 1)
      bAddr <- readArray prg (ptr + 2)
      a <- readArray prg (aAddr)
      b <- readArray prg (bAddr)
      dst <- readArray prg (ptr + 3)
      writeArray prg dst (opcode instr a b)
      run (ptr + 4) prg

opcode :: Int -> Int -> Int -> Int
opcode 1 a b = a + b
opcode 2 a b = a * b
opcode _ _ _ = 42
