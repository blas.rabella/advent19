import Data.List (unfoldr)

main :: IO ()
main = do
  input <- fmap lines getContents
  let masses = map (read) input 
  let fuels = solve1 masses
  let extraFuel = solve2 fuels
  print $ sum fuels
  print extraFuel
  
solve1 :: [Int] -> [Int]
solve1 = map computeFuel

solve2 :: [Int] -> Int
solve2 = sum . map computeExtraFuel

computeFuel :: Int -> Int
computeFuel m = (m `div` 3) - 2

computeExtraFuel :: Int -> Int
computeExtraFuel = sum . unfoldr (\m -> if m <= 0 then Nothing else Just(m, computeFuel m)) 
