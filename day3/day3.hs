import Data.List.Split (splitOn)
import Data.Maybe (catMaybes)

data Direction = U | D| L | R deriving (Show, Read, Eq)
data SegmentDef = SegmentDef Direction Int deriving (Show, Eq)
type Point = (Int, Int)
type Segment = (Point, Point)

main :: IO ()
main = do
  firstLine <- map parseLine . splitOn "," <$> getLine
  secondLine <- map parseLine . splitOn "," <$> getLine
  let segmentsA = genSegment . genVertex $ firstLine
  let segmentsB = genSegment . genVertex $ secondLine
  let (v1, h1) = vertsAndHors  segmentsA
  let (v2, h2) = vertsAndHors segmentsB
  let intersections = catMaybes $ (crossingPoint <$> h1 <*> v2) ++ (crossingPoint <$> h2 <*> v1)
  print $ minimum . map manhattan $ intersections
  print $ minimum . map manhattan $ zip (map (getSteps segmentsA 0) intersections) (map (getSteps segmentsB 0) intersections)

parseLine :: String -> SegmentDef
parseLine s = SegmentDef dir len
  where dir = read . (:[]) . head $ s
        len = read . tail $ s

genVertex :: [SegmentDef] -> [Point]
genVertex = scanl nextPoint (0,0)

nextPoint :: Point -> SegmentDef -> Point
nextPoint (x,y) (SegmentDef U n) = (x, y + n)
nextPoint (x,y) (SegmentDef D n) = (x, y - n)
nextPoint (x,y) (SegmentDef R n) = (x + n, y)
nextPoint (x,y) (SegmentDef L n) = (x - n , y)

flipSegment :: Segment -> Segment
flipSegment (a@(x1,y1), b@(x2,y2))
  | x1 > x2 = (b , a)
  | y2 < y1 = (b , a)
  | otherwise = (a , b)

genSegment :: [Point] -> [Segment]
genSegment ps = zip ps (tail ps)

vertsAndHors :: [Segment] -> ([Segment], [Segment])
vertsAndHors xs = if isVert (head l1) then (l1,l2) else (l2,l1)
  where l1 = [x | (x,i) <- zip xs [0..], even i]
        l2 = [x | (x,i) <- zip xs [0..], odd i]
        isVert ((_,y1), (_,y2)) = y1 /= y2

crossingPoint :: Segment -> Segment -> Maybe Point
crossingPoint a b = crossingPoint' (flipSegment a) (flipSegment b)
  where crossingPoint' ((x1,y1), (x2, _)) ((x3,y3),(_,y4))
          | (y4 > y1) && (y1 > y3) && (x1 < x3) && (x3 < x2) = Just (x3, y1)
          | otherwise = Nothing

manhattan :: Point -> Int
manhattan (a,b) = (abs a) + (abs b)

getSteps :: [Segment] -> Int -> Point -> Int
getSteps (s:ss) acc p = if isInSeg p s then acc + (dist p s) else getSteps ss (acc + (len s)) p
  where len ((x1,y1), (x2,y2)) = abs $ (x2-x1) + (y2-y1)
        dist (x,y) ((x1,y1), _) = abs ((x1-x) + (y1 - y))
getSteps [] _ _ = error "KAboom something you did is wrong"
  
isInSeg :: Point -> Segment -> Bool
isInSeg (a, b) ((x1,y1),(x2,y2)) = a <= max x1 x2 && a >= min x1 x2 && b <= max y1 y2 && b >= min y1 y2
